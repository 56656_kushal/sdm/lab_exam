const express= require('express')
const utils= require('../utils')
const route=express.Router()

route.get('/:name',(req,res)=>
{

    const conn= utils.getConnection()
    const {name}=req.params
    conn.query(`select * from Movie where movie_title='${name}'`,(error,data)=>
    {
        res.send(utils.getResult(error,data))
    })

})

route.post('/',(req,res)=>
{

    const conn= utils.getConnection()
    const {movie_title,movie_release_date,movie_time,director_name}=req.body
    conn.query(`insert into Movie(movie_title,movie_release_date,movie_time,director_name)values('${movie_title}','${movie_release_date}','${movie_time}','${director_name}')`,(error,data)=>
    {
        res.send(utils.getResult(error,data))
    })

})

route.put('/:id',(req,res)=>
{

    const conn= utils.getConnection()
    const{id}=req.params
    const {movie_release_date,movie_time}=req.body
    conn.query(`update Movie set movie_release_date='${movie_release_date}',movie_time='${movie_time}' where movie_id=${id}`,(error,data)=>
    {
        res.send(utils.getResult(error,data))
    })

})


route.delete('/:id',(req,res)=>
{

    const conn= utils.getConnection()
    const{id}=req.params
    conn.query(`delete from Movie where movie_id=${id}`,(error,data)=>
    {
        res.send(utils.getResult(error,data))
    })

})

module.exports=route