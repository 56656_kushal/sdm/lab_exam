const express= require('express')
const cors= require('cors')
const MRouter=require('./routes/movies')
const app = express()

app.use(cors('*'))
app.use(express.json())
app.use(MRouter)


app.listen(4000,'0.0.0.0',()=>
{
    console.log('Started.....')
})